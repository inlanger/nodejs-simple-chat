var port = process.env.PORT || 8000,
	http = require('http'),
    swig  = require('swig'),
    WebSocketServer = require('ws').Server, 
    redis = require("redis"),
    redis_client = redis.createClient(process.env.REDIS_URL);

// Simple page rendering
var server = http.createServer(function(request, response) {  
    response.writeHeader(200, {"Content-Type": "text/html"});  
    redis_client.lrange('messages', 0, -1, function (error, messages) {
    	if (error) throw error;
	    response.write(swig.renderFile('./templates/index.html', {
		    messages: messages.slice(-50, -1)
		}));  
		response.end();  
	});
});
server.listen(port)

var wss = new WebSocketServer({server: server})

// Receiving socket messages
wss.on('connection', function connection(ws) {
	ws.on('message', function incoming(message) {
		wss.clients.forEach(function each(client) {
    		client.send(message);
  		});
		redis_client.lpush("messages", message);
	});
});

// Create new swig filter to parse JSON from Redis DB
swig.setFilter('json_decode', function (input) {
  return JSON.parse(input);
});

// Catch Redis errors
redis_client.on("error", function (err) {
    console.log("Redis error " + err);
});